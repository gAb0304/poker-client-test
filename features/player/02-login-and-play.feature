@player_feature_02
Feature: Players login and Play

  @players_login_and_play
  Scenario: Players login and play up to showdown
    Given Player <num> opens a browser
    Then Player <num> visits "login/id" page
    Then Player <num> waits 2 seconds
    Then Player <num> should see element "loginByIdForm"
    Then Player <num> enters "phoneNumber" to "phone" field
    Then Player <num> enters "password" to "password" field
    Then Player <num> clicks the "submit" button
    Then Player <num> waits 3 seconds
    Then Player <num> should see his name
    Then Player <num> decide to "<decision>"
    Then Player <num> do "move1" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision   |
    | 0      | fold       |
    | 1      | fold       |
    | 2      | call       |
    | 3      | call       |
    | 4      | call       |
    | 5      | call       |

  @players_2nd_move
  Scenario: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "move1" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 3      | check       |
    | 4      | check       |
    | 5      | check       |

  @players_3rd_move
  Scenario: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "move1" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 3      | check       |
    | 4      | check       |
    | 5      | check       |

  @players_4th_move
  Scenario: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "move1" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 3      | check       |
    | 4      | raise       |
    | 5      | call        |

  @players_5th_move
    Scenario: Players login and play up to showdown
      Then Player <num> decide to "<decision>"
      Then Player <num> do "move1" screenshot
      Then Player <num> waits 2 seconds

      Examples: 
      | num    | decision    |
      | 2      | fold        |
      | 3      | fold        |


