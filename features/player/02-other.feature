@open_multiple_browsers
Feature: Multiple Browser

  Scenario:  Open Multiple Browser
    
    Then "<name>" close browser"

      Examples: 
      | name                     | position       | decision        | id                          |
      | Player 1                 | 1              | raise           | 611112591d3e5921f83cd748    |
      | Player 2                 | 2              | call            | 611112d71d3e5921f83cd749    |
      | Player 3                 | 3              | call            | 611112d71d3e5921f83cd74a    |
      | Player 4                 | 4              | call            | 611112d81d3e5921f83cd74b    |
      | Player 5                 | 5              | call            | 611112d91d3e5921f83cd74c    |
      | Player 6                 | 6              | call            | 611112d91d3e5921f83cd74d    |