@player_feature_01
Feature: Players login and Play

  @players_login_and_play
  Scenario Outline: Players login and play up to showdown
    Given Player <num> opens a browser
    Then Player <num> visits "login/id" page
    Then Player <num> waits 2 seconds
    Then Player <num> should see element "loginByIdForm"
    Then Player <num> enters "phoneNumber" to "phone" field
    Then Player <num> enters "_id" to "password" field
    Then Player <num> clicks the "submit" button
    Then Player <num> waits 3 seconds
    Then Player <num> should see his name
    Then Player <num> decide to "<decision>"
    Then Player <num> do "1st_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision   |
    | 2      | raise      |
    | 3      | call       |
    | 4      | call       |
    | 5      | call       |
    | 0      | call       |
    | 1      | call       |

  @players_2nd_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "2nd_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision   |
    | 2      | check      |
    | 3      | check      |
    | 4      | check      |
    | 5      | check      |
    | 0      | check      |
    | 1      | check      |

  @players_3rd_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "3rd_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 3      | check       |
    | 4      | check       |
    | 5      | check       |
    | 0      | check       |
    | 1      | check       |

  @players_4th_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "4th_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 3      | check       |
    | 4      | check       |
    | 5      | check       |
    | 0      | check       |
    | 1      | check       |

  @players_5th_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "5th_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 3      | check       |
    | 4      | check       |
    | 5      | check       |
    | 0      | check       |
    | 1      | check       |

@players_round2_1st_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r2_1st_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 3      | call        |
    | 4      | call        |
    | 5      | call        |
    | 0      | call        |
    | 1      | call        |
    | 2      | check       |

@players_round2_2nd_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r2_2nd_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 1      | raise       |
    | 2      | fold        |
    | 3      | call        |
    | 4      | call        |
    | 5      | call        |
    | 0      | call        |

@players_round2_3rd_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r2_3rd_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 1      | check       |
    | 3      | check       |
    | 4      | check       |
    | 5      | check       |
    | 0      | check       |

@players_round2_4th_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r2_4th_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 1      | check       |
    | 3      | check       |
    | 4      | check       |
    | 5      | check       |
    | 0      | check       |

@players_round3_1st_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r3_1st_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | call        |
    | 3      | call        |
    | 4      | call        |
    | 5      | call        |
    | 0      | call        |
    | 1      | call        |

@players_round3_2nd_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r3_2nd_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | raise       |
    | 3      | fold        |
    | 4      | call        |
    | 5      | call        |
    | 0      | call        |
    | 1      | call        |

@players_round3_3rd_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r3_3rd_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 4      | check       |
    | 5      | check       |
    | 0      | check       |
    | 1      | check       |

@players_round3_4th_move
  Scenario Outline: Players login and play up to showdown
    Then Player <num> decide to "<decision>"
    Then Player <num> do "r3_4th_move" screenshot
    Then Player <num> waits 2 seconds

    Examples: 
    | num    | decision    |
    | 2      | check       |
    | 4      | check       |
    | 5      | check       |
    | 0      | check       |
    | 1      | check       |  