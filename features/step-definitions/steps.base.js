const {Given, When, Then, Before, After, setDefaultTimeout} = require('@cucumber/cucumber');
const {expect} = require('chai');
const {By} = require('selenium-webdriver');
const Support = require('../support/helpers');
const AdminSupport = require('../support/helpers.admin');
const helpers = new Support();
const adminHelpers = new AdminSupport();

const DEFAULT_TIMEOUT = 300 * 1000;

setDefaultTimeout(DEFAULT_TIMEOUT);

// After(async ()=>{
//   await helpers.removeTempFiles();
// })

Given('{string} {string} open(s) a browser', async function (user, userType) {
  await helpers.initDriver(user, userType);
});

Given('{string} open(s) the {string} page', async function(user, page) {
  await helpers.initDriver(user, page);
});

Given('{string} can see {string}', async function(user, text) {
  const driver = await helpers.getDriver(user);
  await helpers.findText(driver, text);
});

Given('{string} is on the table', async function (user) {
  const driver = await helpers.getDriver(user);
  await helpers.findText(driver, user);
});

Given('{string} browser is open', async function (user) {
  const driver = await helpers.getDriver(user);
  driver.quit()
});

Given('Tournament not started yet', function () {
  helpers.checkSchedule();
});

Given('{string} has open browser', async function (user) {
  const driver = await helpers.getDriver(user);
});

When('{string} on {int} position', async function (user, position) {
  const driver = await helpers.getDriver(user);
  
});

When('{string} click(s) the {string} button', async function (user, elemId) {
  const driver = await helpers.getDriver(user);
  await driver.findElement(By.id(elemId)).click();
});

When('{string} click the {string} button', async function (user, elemId) {
  const driver = await helpers.getDriver(user);
  await driver.findElement(By.id(elemId)).click();
});

When('{string} can see alert message', async function(user) {
  const driver = await helpers.getDriver(user);
  const alertMsg = await helpers.findAlertMsgText(driver);
  expect(alertMsg).to.empty
});

When('{string} current turn', async function(user) {
  const driver = await helpers.getDriver(user);
  await helpers.findElementById(driver, 'betOptions');
});

Then('{string} should inform player when tournament is ready', async function (user) {
  const driver = await helpers.getDriver(user);
  await helpers.waitTime(driver);
});

Then('{string} close browser', async function (user, move) {
  const driver = await helpers.getDriver(user);
  driver.quit();
});

Then('{string} visit(s) {string} page', async function (user, page) {
  const driver = await helpers.getDriver(user);
  await helpers.visitPage(driver, page);
});

Then('{string} set event time in {int} {string}', async function (user, num, string) {
  const driver = await helpers.getDriver(user);
  await helpers.setEventTime(driver, num, string);
});

Then('{string} enter(s) the admin credentials', async function (user) {
  const driver = await helpers.getDriver(user);
  await helpers.adminHelpers.loginAsAdmin(driver, user);
});

Then('{string} should see element {string}', async function(user, elemId) {
  const driver = await helpers.getDriver(user);
  driver.findElement(By.id(elemId));
})

Then('{string} fill(s) up the form', async (user) => {
  const driver = await helpers.getDriver(user);
  await helpers.adminHelpers.loginAsAdmin(driver, user);
});

Then('{string} should see {string}', async function(user, text) {
  const driver = await helpers.getDriver(user);
  await helpers.findText(driver, text);
})

Then('{string} wait(s) {int} second(s)', async function(user, timeout) {
  const driver = await helpers.getDriver(user);
  await driver.sleep(timeout * 1000);
});

Then('{string} enter(s) {string} to {string} field', async function (user, value, elemId) {
  const driver = await helpers.getDriver(user);
  await driver.findElement(By.id(elemId)).sendKeys(value);
});

Then('{string} do {string} screenshot', async function(user, name) {
  const driver = await helpers.getDriver(user);
  await helpers.takeScreenshot(driver, user, name);
});

Then('{string} close the browser', async function(user) {
  const driver = await helpers.getDriver(user);
  await helpers.takeScreenshot(driver, user);
  driver.quit();
});

Then('{string} should see alert message', async function(user) {
  const driver = await helpers.getDriver(user);
  const text = await driver.switchTo().alert().getText();
});

Then('{string} add {int} player(s)', async function (user, count) {
  const driver = await helpers.getDriver(user);
  await helpers.adminHelpers.addPlayers(driver, count);
});

Then('{string} should see {string} message', async function (user, message) {
  const driver = await helpers.getDriver(user);
  const alertMsg = await helpers.findAlertMsg(driver);
  alertMsg.accept();
});

Then('{string} should confirm alert message', async function (user) {
  const driver = await helpers.getDriver(user);
  const alertMsg = await helpers.findAlertMsg(driver);
  alertMsg.accept();
});

Then('{string} click the {string} button', async function (user, elemId) {
  const driver = await helpers.getDriver(user);
  await driver.findElement(By.id(elemId)).click();
});

Then('{string} get players order sitting', async function (user) {
  const driver = await helpers.getDriver(user);
  await helpers.adminHelpers.getOrderSitting(driver);
});

Then('{string} clears {string} field', async function (user, elemId) {
  const driver = await helpers.getDriver(user);
  await helpers.clearField(driver, elemId);
});

