const {Given, When, Then, Before, After, setDefaultTimeout} = require('@cucumber/cucumber');
const {expect} = require('chai');
const {By} = require('selenium-webdriver');
const Support = require('../support/helpers');
const helpers = new Support();

const DEFAULT_TIMEOUT = 60000;

Given('Player {int} opens a browser', async function (num) {
  await helpers.initDriver(null, 'player', num);
});

Then('Player {int} visit(s) {string} page', async function (num, page) {
  const driver = await helpers.getDriver('', 'player', num);
  await helpers.visitPage(driver, page);
});

Then('Player {int} wait(s) {int} seconds', async function (num, timeout) {
  const driver = await helpers.getDriver('null', 'player', num);
  await driver.sleep(timeout * 1000);
});

Then('Player {int} should see element {string}', async function (num, elemId) {
  const driver = await helpers.getDriver('null', 'player', num);
  driver.findElement(By.id(elemId));
});

Then('Player {int} enter(s) {string} to {string} field', async function (num, objName, elemId) {
  const driver = await helpers.getDriver('null', 'player', num);
  await helpers.inputText(driver, elemId, null, 'player', num, objName);
});

Then('Player {int} click(s) the {string} button', async function (num, elemId) {
  const driver = await helpers.getDriver('null', 'player', num);
  await driver.findElement(By.id(elemId)).click(); 
});

Then('Player {int} should see his name', async function (num) {
  const driver = await helpers.getDriver('null', 'player', num);
  await helpers.findText(driver, null, 'player', num);
});

Then('Player {int} decide to {string}', async function (num, move) {
  const driver = await helpers.getDriver('null', 'player', num);
  await helpers.playerMove(driver, move);
});

Then('Player {int} do {string} screenshot', async function (num, name) {
  const driver = await helpers.getDriver('null', 'player', num);
  await helpers.takeScreenshot(driver, null, name, 'player', num);
});