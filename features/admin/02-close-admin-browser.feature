Feature: Close admin and players browser

  Scenario: Login as Administrator
    Given "<name>" browser is open
    
  Examples: 
    | name                     | phone_number                     | password                                |
    | Hezi                     | 0                                |                                         |
    # | Player 1                 | 1                                | 611112591d3e5921f83cd748                |
    # | Player 2                 | 2                                | 611112d71d3e5921f83cd749                |
    # | Player 3                 | 3                                | 611112d71d3e5921f83cd74a                |
    # | Player 4                 | 4                                | 611112d81d3e5921f83cd74b                |
    # | Player 5                 | 5                                | 611112d91d3e5921f83cd74c                |
    # | Player 6                 | 6                                | 611112d91d3e5921f83cd74d                |