@admin_feature
Feature: Admin feature

  # @Test
  # Scenario: Test
  #   Then Test
    
  @admin_login
  Scenario: Login as Administrator
    Given "Hezi" "admin" opens a browser
    Then "Hezi" visits "login" page
    Then "Hezi" waits 5 seconds
    Then "Hezi" should see element "loginForm"
    Then "Hezi" enters the admin credentials
    When "Hezi" clicks the "submit" button
    Then "Hezi" waits 2 seconds
    Then "Hezi" should see "* Generate event *"

  @generate_tournament
  Scenario: Fill up event information and create tournament
    Given "Hezi" can see "* Generate event *"
    Then "Hezi" should see element "eventInfoSection"
    Then "Hezi" set event time in 0 "minutes"
    Then "Hezi" waits 2 seconds
    Then "Hezi" clears "playerCount" field
    Then "Hezi" enters "6" to "playerCount" field
    When "Hezi" clicks the "createBtn" button
    Then "Hezi" waits 2 seconds
    Then "Hezi" should see element "addPlayersSection"
    Then "Hezi" add 6 players
    Then "Hezi" waits 40 seconds
    Then "Hezi" do "players_list" screenshot
    When "Hezi" clicks the "nextBtn" button
    Then "Hezi" waits 2 seconds
    Then "Hezi" should see element "configSection"
    When "Hezi" clicks the "saveBtn" button
    Then "Hezi" waits 2 seconds
    Then "Hezi" should see "Tournament updated successfully" message
    Then "Hezi" get players order sitting
    Then "Hezi" waits 5 seconds


  # NOTE: On line 29, waiting time depends on how many players will be on the table. 2 seconds each player plus 3 seconds allowance