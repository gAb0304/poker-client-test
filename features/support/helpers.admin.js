class AdminHelpers {
  constructor(helpers) {
    this.helpers = helpers;
  }

  async loginAsAdmin(driver, user) {
    const name = user.toLowerCase();
    const { admins = {} } = this.helpers.accounts || {};
    const { email = '', password = '' } = admins[name] || {};

    await this.helpers.api.login(email, password, (token)=> this.helpers.token = token);
    await this.helpers.typeInField(driver, 'email', email);
    await this.helpers.typeInField(driver, 'password', password);
  }

  async getOrderSitting() {
    this.getTournament(async (tournament)=> {
      const tableId = tournament.activeTables[0];
      this.getTable(tableId);
      this.getPlayersData(tableId)
    });
  }

  async getPlayersData(tableId) {
    await this.helpers.api.getActivePlayers(this.helpers.token, { gameId: tableId}, async (data)=> {
      await this.helpers.removeTempFiles(this.helpers.playerListTempPath);
      let jsonData = JSON.stringify(data);
      jsonData = `module.exports = ${jsonData}` 
      this.helpers.createFile(`${this.helpers.playerListTempPath}`, jsonData);
    })
  }

  async getTable(tableId) {
    await this.helpers.api.getTable(this.helpers.token, tableId, (table)=>{
      const { miniRoundActivePlayers = [] } = table || {};
      let text = '';
      miniRoundActivePlayers.map(txt => text += txt + '\n');
    });
  }

  async getTournament(callback=()=>null) {
    this.helpers.api.getTournaments(this.helpers.token, (tournaments)=> {
      const dataLen = tournaments.length-1;
      return callback(tournaments[dataLen])
    });
  }
  
  async addPlayers(driver, playerCount) {
    const playerLen = this.helpers.players.length;
    if (!playerLen || playerLen < playerCount) {
      throw new Error(`HelpersAdmin.addPlayers(): Players not configured properly`);
    }

    this.inputRowPlayerInfo(driver, this.helpers.players[0], 0);
    let counter = 1;
    const addingPlayer = setInterval(async ()=> {
      console.log(`addPlayers Timer(): ${counter}`)
      this.inputRowPlayerInfo(driver, this.helpers.players[counter], counter);

      counter++;
      if (playerCount-1 < counter) clearInterval(addingPlayer);
    },5000)
   
  }

  async inputRowPlayerInfo(driver, data, index) {
    const { phoneNumber = '', name = '' } = data || {};
    let fullName='';
    await this.helpers.typeInField(driver, `phoneNumber${index}`, phoneNumber, index);
    fullName = await this.helpers.getElementText(driver, `fullName${index}`, index);
    console.log(`inputRowPlayerInfo(): fullname: ${fullName}, index: ${index}`)
    if (fullName) {
      try {
        console.log(`inputRowPlayerInfo(): fullname found, index: ${index}`)
        await this.helpers.click(driver, `addPlayerBtn${index}`, null, index);
        await this.helpers.acceptAlert(driver, index);
      } catch (err) {
        if (err.message !== 'no such alert') {
          throw new Error(`HelpersAdmin.inputRowPlayerInfo(): failed to add player, error: `, err.message) 
        }
      }
    } else {
      console.log(`inputRowPlayerInfo(): fullname not found, index: ${index}`)
      await this.helpers.typeInField(driver, `fullName${index}`, name, index);
      await this.helpers.click(driver, `registerPlayerBtn${index}`, index);
    }
  }
}

module.exports = AdminHelpers;