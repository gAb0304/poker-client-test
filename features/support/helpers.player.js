class PlayerHelpers{
  constructor(helpers) {
    this.helpers = helpers;
  }

  getPlayerName(index) {
    try {
      const playerData = this.getPlayerData(index);
      const { firstName = '', lastName = '' } = playerData || {};
      const name = `${firstName} ${lastName}`;
      this.helpers.playerName = name;
      return name;
    } catch (err) {
      throw new Error(err)
    }
    return null;
  }

  getPlayerData(index) {
    try {
      const players = require('../../data/players');
      return players[index];
    } catch(err) {
      throw new Error(`HelpersPlayer.getPlayerData(): Player data not found`);
    }
  }

}

module.exports = PlayerHelpers;
