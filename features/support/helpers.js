const webdriver = require('selenium-webdriver');
const { expect } = require('chai');
const moment = require('moment');
const accounts = require('./../../data/accounts');
const config = require('../../config');
const { By, Key } = require('selenium-webdriver');
const fs = require("fs"); 

const Controllers = require('../controllers');
const PlayerHelper = require('./helpers.player');
const AdminHelper = require('./helpers.admin');

module.exports = class {
  constructor() {
    this.drivers = {};
    this.config = config || {};
    this.accounts = accounts || {};
    this.players = accounts && accounts.players || [];
    this.frontendUrl = config && config.frontendUrl || '';
    this.screenshotsPath = config && config.screenshotsPath || '';
    this.playerListTempPath = config && config.playerListTempPath || '';
    this.schedule = '';
    this.playerWindow = config && config.player && config.player.window || [];
    this.playerIndex=0;
    this.orderSitting=[];
    this.playerName='';
    this.api = new Controllers();
    this.playerHelpers = new PlayerHelper(this);
    this.adminHelpers = new AdminHelper(this);
  }
  
  async sleep(driver, timeout=1000) {
    await driver.sleep(timeout);
  }

  async initDriver(user, userType, index) {
    const { browser = 'chrome' } = this.config || {};
    const driver = await new webdriver.Builder()
      .forBrowser(browser)
      .build();
    await driver.manage().window().maximize();

    const name = await this.getUsername(user, userType, index);
    this.drivers[name]=driver;
    return driver;
  }

  async getUsername(user, userType, index) {
    if (userType==='player') {
      return await this.playerHelpers.getPlayerName(index);
    }
    return await user;
  }

  async getDriver(user, userType, int) {
    if (typeof(user) === 'string') {
      if (userType!=='player') {
        return this.drivers[user];
      } else {
        const name = await this.getUsername(user, userType, int);
        if (this.drivers[name]) {
          return this.drivers[name];
        }
      }
    } else {
      if (user) {
        return user;
      }
    }
    return this.drivers;
  }

  visitPage(driver, page) {
    if (this.frontendUrl) {
      driver.get(`${this.frontendUrl}/${page}`);
    }
  }

  createFile(path, val) {
    console.log(`createFile`)
    fs.writeFile(path, val, function(err) {
      if(err) {
        console.error(`Helpers.createFile(): Error creating file at path: ${path}`, err);
      }
    }); 
  }

  readFile(path, callback=()=>null) {
    fs.readFile(path, (err, data) => {
      if (err) throw err;
      return callback(data);
    });
  }
 
  async findText(driver, value, userType, num) {
    const playerName = await this.getUsername(null, userType, num);
    const val = userType==='player'? playerName : value;

    let el = await driver.findElement(By.xpath("//*[contains(text(),'" + val + "')]"));
    expect(el).to.not.equal(null);
  }

  async inputText(driver, elemId, value, userType, index, objName) {
    let inputVal = value;
    if (userType==='player') {  
      const playerData = this.playerHelpers.getPlayerData(index);
      inputVal = playerData[objName];
    }
    return await driver.findElement(By.id(elemId)).sendKeys(inputVal);
  }

  async clickButton(driver, elemId) {
    return await driver.findElement(By.id(elemId)).click();
  }

  async clearField(driver, elemId) {
    return await driver.findElement(By.id(elemId)).clear();
  }

  async findElementById(driver, elemId) {
    return await driver.findElement(By.id(elemId));
  }

  findAlertMsg(driver) {
    return driver.switchTo().alert();
  }

  async findAlertMsgText(driver) {
    return driver.switchTo().alert().getText();
  }

  async takeScreenshot(driver, user, title='', userType='', index) {
    const username = await this.getUsername(null, userType, index);
    const prefixName = userType==='player'? `${username}_` : `${user}_`;
    if (!fs.existsSync(`./${this.screenshotsPath}`)){
      fs.mkdir(`./${this.screenshotsPath}`, function(err) {
        if (err) {
          console.log(err)
        }
      })
    }

    let path = `${this.screenshotsPath}/${prefixName}${title}.png`;
    let counter = 1;

    while (fs.existsSync(path)) {
      path =`${this.screenshotsPath}/${prefixName}${title}_${counter}.png`;
      counter++;
    }

    driver.takeScreenshot().then(
      function(image, err) {
        require('fs').writeFile(path, image, 'base64', function(err) {
          console.log(err);
        });
    })
  }

  

  async setEventTime(driver, num, key) {
    const time = moment(new Date).add(num, key);
    this.schedule = time.format('mm00')// minute second before tournament start
    await this.inputText(driver, 'time', `${time.format('hhmma')}`);
  }

  async playerMove(driver, elemId) {
    try {
      const elementId = elemId==='raise'?'raiseId': elemId;
      await this.clickButton(driver, elementId);
    } catch (err) {
      console.error(`Helpers.playerMove(): Player ${this.playerName} failed to ${elemId}, error: `, err.message)
    }
  }

  checkSchedule() {
    const time = moment(new Date()).format('mmss');
    expect(parseInt(this.schedule)).to.be.least(parseInt(time));
  }

  async waitTime(driver) {
    const time = moment(new Date()).format('mmss');
    const waitTime = parseInt(this.schedule) - parseInt(time);
    await this.sleep(driver)
  }

  async removeTempFiles(path) {
    console.log(`Helpers.removeTempFiles(): `, fs.existsSync(`./${path}`))
    if (fs.existsSync(`./${path}`)) {
      await fs.unlink(path, function(err) {
        if (err) {
          console.error(`Helpers.removeTempFiles(): failed to remove a file, error: `, err);
        } else {
          console.log(`Helpers.removeTempFiles(): successfully removed file at: ${path}`)
        }
      });
    }
  }

  async getElementText(driver, elemId, index) {
    console.log(`Helpers.getElementText(): Player ${index} elemId: ${elemId}`);
    return await driver.findElement(By.id(elemId)).getText();
  }

  async typeInField(driver, elemId, value, index='') {
    console.log(`Helpers.typeInField(): Player ${index} elemId: ${elemId} value: ${value}`);
    await this.sleep(driver, 200);
    const el = await driver.findElement(By.id(elemId));
    await el.click();
    await this.sleep(driver, 200);
    await el.sendKeys(Key.HOME, Key.chord(Key.SHIFT, Key.END), value);
    await this.sleep(driver, 1000);
  }

  async findElement(driver, elemId, fieldname = 'id', xpath='') {
    if (!xpath) {
      // if not getting by xpath directly
      xpath = '//*[@' + fieldname + '="' + elemId + '"]';
    }
    return await driver.findElement(By.xpath(xpath));
  }

  async findElementById(driver, elemId) {
    return await driver.findElement(By.id(elemId));
  }

  async scrollTo(driver, elemId, fieldname='id') {
    const el = await this.findElement(driver, elemId, fieldname);
    await driver.executeScript("arguments[0].scrollIntoView()", el);
    await this.sleep(driver);
  }

  async click(driver, elemId, noscroll=false, index) {
    console.log(`Helpers.click(): Player ${index} elemId: ${elemId}`);
    await this.sleep(driver, 200);
    const el = await this.findElement(driver, elemId);
    if (!noscroll) {
      await this.scrollTo(driver, elemId).catch((error) => {
        console.error(`Helpers.click(): scroll error: `, error);
        throw error;
      });
    }
    await this.sleep(driver, 200);
    await el.click().catch((error) => {
      console.error(`Helpers.click(): failed to click on ${elemId}, error: `, error);
    });
  }

  async acceptAlert(driver, index) {
    console.log(`Helpers.acceptAlert(): Player ${index}`);
    await this.sleep(driver, 500);
    await driver.switchTo().alert().accept();
  }
}