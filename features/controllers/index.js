const axios = require('axios');
const config = require('../../config');

module.exports = class {
  constructor() {
    this.backendUrl = config.backendUrl || '';
    this.token = '';
  }

  login(email, password, callback=()=>null) {
    axios.post(`${this.backendUrl}/login`, {
      email,
      password
    })
    .then(function (response) {
      const { data = {} } = response || {};
      const { token = '' } = data || {};
      return callback(token)
    })
    .catch(function (error) {
      console.error(`Controllers.login(): failed to login, error: `, error);
    });
  }

  getTournaments(token, callback=()=>null) {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token
    }

    axios.get(`${this.backendUrl}/tournament`, { headers })
    .then(function (response) {
      const { data = [] } = response || {};
      return callback(data);
    })
    .catch(function (error) {
      console.error(`Controllers.getTournaments(): failed to login, error: `);
    });
  }

  getTable(token, tableId, callback=()=>null) {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token
    }

    axios.get(`${this.backendUrl}/getTable/${tableId}`, { headers } )
    .then(function (response) {
      const { data = null } = response || {};
      return callback(data);
    })
    .catch(function (error) {
      console.error(`Controllers.getActivePlayers(): Error fetching table`, error.request.error);
    });
  }

  getActivePlayers(token, data, callback=()=>null) {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': token
    }

    axios.post(`${this.backendUrl}/getActivePlayers/`, data, { headers } )
    .then(function (response) {
      const { data = null } = response || {};
      return callback(data);
    })
    .catch(function (error) {
      console.error(`Controllers.getActivePlayers(): Error fetching active players`, error);
    });
  }
}